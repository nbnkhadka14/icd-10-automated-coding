from flask import Flask, render_template, request, jsonify
import logging as log
import pickle
import pandas as pd
import os

app = Flask(__name__)

NGRAM_MODEL_FILE = 'ngram.model'
ICD_10_CODES_FILE = 'codes.csv'
VECTOR_FILE_TO_LOAD = 'vectors.txt'
VECTOR_ZIP = 'vectors.txt.zip'


# LOAD NGRAM
import json
import operator

def load_model():
    with open(NGRAM_MODEL_FILE, 'rb') as f:
        return pickle.load(f)
bigram_dict = load_model()


def get_suggestion(word, only_word=False):
    record = bigram_dict.get(word.lower())
    if record:
        sorted_record = sorted(record.items(), key=operator.itemgetter(1), reverse=True)
        if only_word:
            return [x[0] for x in sorted_record]
        return sorted_record

# LOAD SIMILARITY
import difflib
from tqdm import tqdm
codes = pd.read_csv(ICD_10_CODES_FILE, usecols=['Category Code', 'Full Code', 'Full Description'])
full_descriptions = list(codes['Full Description'])


def get_similarity_suggestion(user_input, best_only=False):
    log.warn('starting suggestion')
    list_of_sim = dict()
    for sample_desc in tqdm(full_descriptions):
        seq = difflib.SequenceMatcher(None, sample_desc, user_input)
        d = seq.quick_ratio() * 100
        if best_only and int(d) < 80:
            continue
        list_of_sim.update({sample_desc: float("%.2f" % d)})
    sorted_x = sorted(list_of_sim.items(), key=lambda kv: kv[1], reverse=True)
    to_return = list()
    for each in sorted_x[:5]:
        suggestion = codes.loc[codes['Full Description'] == each[0]][['Full Code', 'Full Description']]
        suggestion = list(suggestion.values[0]) + [each[1]]
        to_return.append(suggestion)
    return to_return


# LOAD GLOVE MODEL FOR WORD SIMILARITY
import gensim

def unzip_vector():
    if not os.path.isfile(VECTOR_FILE_TO_LOAD):
        import zipfile
        with zipfile.ZipFile(VECTOR_ZIP, 'r') as zip_ref:
            zip_ref.extractall('.')
            os.remove(VECTOR_ZIP)

def load_glove_model():
    unzip_vector()
    model = gensim.models.KeyedVectors.load_word2vec_format(VECTOR_FILE_TO_LOAD,binary=False)
    return model

model = load_glove_model()


def similar_word(input_word='kidney', topn=10):
    try:
        return [x[0] for x in model.most_similar(positive=[input_word], topn=topn)]
    except Exception as ex:
        log.error('similar_word; error while getting similar word; error=%s' % str(ex))
    return list()


@app.route('/', methods=['GET', 'POST'])
def index():
    unzip_vector()
    ngram_size = os.path.getsize(NGRAM_MODEL_FILE)/1024/1024
    glove_size = os.path.getsize(VECTOR_FILE_TO_LOAD)/1024/1024
    glove_shape = pd.read_csv(VECTOR_FILE_TO_LOAD).shape[0]
    return render_template(
        'index.htm',
        ngram_model= "%.2f" % ngram_size,
        glove_model= "%.2f" % glove_size,
        glove_shape= glove_shape
    )


@app.route('/similar', methods=['POST'])
def similar():
    word = request.form.get('similar').lower()
    return jsonify(similar_word(word))


@app.route('/suggest', methods=['POST'])
def suggest():
    word = request.form.get('word')
    suggestions = get_suggestion(word, only_word=True)
    if suggestions:
        return jsonify(suggestions[:6])
    return 'No match found'

@app.route('/generate', methods=['POST'])
def generate():
    phrase = request.form.get('phrase')
    if phrase:
        best_only = request.form.get('onlyBest')
        best_only = True if best_only == 'true' else False
        phrase = phrase.strip().lower()
        suggestions = get_similarity_suggestion(phrase, best_only=bool(best_only))
        return jsonify(suggestions)
    return 'Something suggested'

if os.getcwd().startswith('/Users/nabin'):
    app.run(debug=True)
