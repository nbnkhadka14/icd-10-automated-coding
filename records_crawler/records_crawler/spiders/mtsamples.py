# -*- coding: utf-8 -*-
import scrapy


class MtsamplesSpider(scrapy.Spider):
    name = 'mtsamples'
    allowed_domains = ['mtsamples.com']
    start_urls = ['http://mtsamples.com/']

    def parse(self, response):
        lis = response.xpath("//ul/li")
        for li in lis:
            path = li.xpath("a/@href").extract()
            if path and path[0].startswith('/site/pages/browse.asp'):
                request = scrapy.Request(url="https://www.mtsamples.com/%s" % path[0], callback=self.parse_second_page)
                yield request

    def parse_second_page(self, response):
        report_page_links = response.xpath('//a')
        for tr in report_page_links:
             tr = tr.xpath('@href')
             if tr:
                 tr = tr[0].extract()
                 if '/site/pages/sample.asp?' in tr:
                     link = "https://www.mtsamples.com/%s" % tr
                     yield scrapy.Request(url=link, callback=self.parse_report_page)

    def parse_report_page(self, response):
        
        texts = response.xpath("//div[@id='sampletext']/text()").extract()
        collected = ''.join([text.strip() for text in texts]).lower() \
                        .replace('.', '') \
                        .replace('-', '') \
                        .replace(';', '') \
                        .replace('  ', ' ') \
                        .replace('_', '')
        yield {'data': collected}